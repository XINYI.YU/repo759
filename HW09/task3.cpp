#include "mpi.h"
#include <iostream>

int main(int argc, char* argv[]) {
    int rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank); 
    MPI_Status status;       

    size_t n = std::atoi(argv[1]);
    float *arr_send = new float[n];
    float *arr_recv = new float[n];
    for(size_t i=0; i<n; i++)
        arr_send[i] = i;
    
    if (rank==0) {
        double start_time = MPI_Wtime();
        MPI_Send(arr_send, n, MPI_FLOAT, 1, 0, MPI_COMM_WORLD);
        MPI_Recv(arr_recv, n, MPI_FLOAT, 1, 0, MPI_COMM_WORLD, &status);
        double end_time = MPI_Wtime();
        double t0 = (end_time - start_time) * 1000;

        double t1;
        MPI_Recv(&t1, 1, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD, &status);

        std::cout << t0+t1 << "\n"; 
    }
    else if(rank==1){

        double start_time = MPI_Wtime();
        MPI_Recv(arr_recv, n, MPI_FLOAT, 0, 0, MPI_COMM_WORLD, &status);
        MPI_Send(arr_send, n, MPI_FLOAT, 0, 0, MPI_COMM_WORLD);
        double end_time = MPI_Wtime();
        double t1 = (end_time - start_time) * 1000;
        MPI_Send(&t1, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    }

    MPI_Finalize();

    delete []arr_send;
    delete []arr_recv;

}
