#include "montecarlo.h"
#include <omp.h>
#include <iostream>
#include <random>
#include <chrono>
using std::chrono::high_resolution_clock;
using std::chrono::duration;
using std::chrono::duration_cast;


int main(int argc, char* argv[]){

    const size_t n = std::atoi(argv[1]);
    const size_t t = std::atoi(argv[2]);
    float r = 1.0;

    float *x = new float[n];
    float *y = new float[n];

    unsigned seed = high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine randGen(seed);
    std::uniform_real_distribution<float> uniform_dist(-r, r);

    for(size_t i=0; i<n; i++){
        x[i] = uniform_dist(randGen);
        y[i] = uniform_dist(randGen);
    }

    omp_set_num_threads(t);
    double start_time = omp_get_wtime();
    int incircle = montecarlo(n, x, y, r);
    double end_time = omp_get_wtime();

    double pi = 4.0*incircle / n;

    std::cout << pi << "\n";
    std::cout << (end_time-start_time)*1000 << "\n";

    delete[] x;
    delete[] y;

}

