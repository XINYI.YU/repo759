#include "cluster.h"
#include <cmath>
#include <iostream>


void cluster(const size_t n, const size_t t, const float *arr, const float *centers, float *dists) {

    float accumulator=0.0f;

    #pragma omp parallel num_threads(t) firstprivate(accumulator)
    {
        unsigned int tid = omp_get_thread_num();

        #pragma omp for
        for (size_t i = 0; i < n; i++) 
            accumulator += std::abs(arr[i] - centers[tid]);
       
        dists[tid] = accumulator;
    }

}
