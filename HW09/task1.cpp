#include "cluster.h"
#include <omp.h>
#include <iostream>
#include <random>
#include <chrono>
#include <algorithm>
using std::chrono::high_resolution_clock;
using std::chrono::duration;
using std::chrono::duration_cast;



int main(int argc, char* argv[]){

    const size_t n = std::atoi(argv[1]);
    const size_t t = std::atoi(argv[2]);

    float *arr = new float[n];
    unsigned seed = high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine randGen(seed);
    std::uniform_real_distribution<float> uniform_dist(0., n);
    for(size_t i=0; i<n; i++)
        arr[i] = uniform_dist(randGen);
    std::sort(arr, arr+n);


    float *centers = new float[t];
    for(size_t i=1; i<=t; i++)
        centers[i-1] = static_cast<float>((2*i-1)*n) / (2*t);


    float *dists = new float[t];

    double start_time = omp_get_wtime();
    cluster(n, t, arr, centers, dists);
    double end_time = omp_get_wtime();


    float dist_max = dists[0];
    size_t t_max = 0;
    for(size_t i=1; i<t; i++){
        if(dists[i] > dist_max){
            dist_max = dists[i];
            t_max = i;
        }
    }

    std::cout << dist_max << "\n";
    std::cout << t_max << "\n";
    std::cout << 1000*(end_time - start_time) << "\n";

    delete[] arr;
    delete[] centers;
    delete[] dists;

}



