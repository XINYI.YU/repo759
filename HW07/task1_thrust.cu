#include <iostream>
#include <stdlib.h>
#include "time.h"
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/reduce.h>
#include <thrust/functional.h>
#include <thrust/random.h>
#include <iostream>
#include <chrono>

int main(int argc, char* argv[]) {
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " n" << std::endl;
        return 1;
    }

    int n = std::atoi(argv[1]);
    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    // Create a random host vector
    thrust::host_vector<float> h_vec(n);

    // fill the vector with random float from -1.0 to 1.0
    srand((int)time(0));
    for (unsigned int m1=0; m1<n; m1++) h_vec[m1] = -1.0+1.0*rand()/RAND_MAX*2.0;

    // Copy host vector to device
    thrust::device_vector<float> d_vec = h_vec;

    // Perform reduction using Thrust


    cudaEventRecord(start);
    float result = thrust::reduce(d_vec.begin(), d_vec.end(), 0.0f, thrust::plus<float>());
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    // Print the result and execution time
    std::cout << result << std::endl;

    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    std::cout << milliseconds << std::endl;

    // Clean up
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    return 0;
}
