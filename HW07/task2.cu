#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <iostream>
#include <chrono>
#include "count.cuh"

int main(int argc, char* argv[]) {
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " n" << std::endl;
        return 1;
    }

    int n = std::atoi(argv[1]);

    // Create a random host vector with integers in the range [0, 500]
    thrust::host_vector<int> h_vec(n);
    srand((int)time(0));
    for (int m1=0; m1<n; m1++) h_vec[m1] = (int)(1.0*rand()/RAND_MAX*501.0);

    // Copy host vector to device
    thrust::device_vector<int> d_vec = h_vec;

    // Allocate output vectors
    thrust::device_vector<int> values;
    thrust::device_vector<int> counts;

    // Perform counting operation
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start);
    count(d_vec, values, counts);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    // Print the last element of values and counts
    std::cout << values[values.size() - 1] << std::endl;
    std::cout << counts[counts.size() - 1] << std::endl;

    // Print the time taken to run the count function
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    std::cout << milliseconds << std::endl;

    // Clean up
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    return 0;
}

