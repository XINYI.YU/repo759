#define CUB_STDERR // print CUDA runtime errors to console
#include <stdio.h>
#include <stdlib.h>
#include "time.h"
#include <cub/util_allocator.cuh>
#include <cub/device/device_reduce.cuh>
#include "cub/util_debug.cuh"
using namespace cub;
CachingDeviceAllocator  g_allocator(true);  // Caching allocator for device memory

int main(int argc, char* argv[]) {
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " n" << std::endl;
        return 1;
    }

    int n = std::atoi(argv[1]);
    const size_t num_items = n;

    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    float* h_in = new float[num_items];

    srand((int)time(0));
    for (unsigned int m1=0; m1<num_items; m1++)  h_in[m1] = -1.0+1.0*rand()/RAND_MAX*2.0;
    
    // Set up device arrays
    float* d_in = NULL;
    // CubDebugExit(g_allocator.DeviceAllocate((void**)& d_in, sizeof(int) * num_items));
    g_allocator.DeviceAllocate((void**)& d_in, sizeof(float) * num_items);

    // Initialize device input
    // CubDebugExit(cudaMemcpy(d_in, h_in, sizeof(float) * num_items, cudaMemcpyHostToDevice));
    cudaMemcpy(d_in, h_in, sizeof(float) * num_items, cudaMemcpyHostToDevice);
    // Setup device output array
    float* d_sum = NULL;
    // CubDebugExit(g_allocator.DeviceAllocate((void**)& d_sum, sizeof(int) * 1));
    g_allocator.DeviceAllocate((void**)& d_sum, sizeof(float) * 1);
    // Request and allocate temporary storage
    void* d_temp_storage = NULL;
    size_t temp_storage_bytes = 0;
    // CubDebugExit(DeviceReduce::Sum(d_temp_storage, temp_storage_bytes, d_in, d_sum, num_items));
    DeviceReduce::Sum(d_temp_storage, temp_storage_bytes, d_in, d_sum, num_items);
    // CubDebugExit(g_allocator.DeviceAllocate(&d_temp_storage, temp_storage_bytes));
    g_allocator.DeviceAllocate(&d_temp_storage, temp_storage_bytes);

    // Do the actual reduce operation
    cudaEventRecord(start);
    // CubDebugExit(DeviceReduce::Sum(d_temp_storage, temp_storage_bytes, d_in, d_sum, num_items));
    DeviceReduce::Sum(d_temp_storage, temp_storage_bytes, d_in, d_sum, num_items);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float gpu_sum;
    // CubDebugExit(cudaMemcpy(&gpu_sum, d_sum, sizeof(int) * 1, cudaMemcpyDeviceToHost));
    cudaMemcpy(&gpu_sum, d_sum, sizeof(float) * 1, cudaMemcpyDeviceToHost);
    // calculate the elasped time    
    float ms;
    cudaEventElapsedTime(&ms, start, stop);
    // Check for correctness
    printf("%f\n%f\n",gpu_sum,ms);

    // Cleanup
    if (d_in) CubDebugExit(g_allocator.DeviceFree(d_in));
    if (d_sum) CubDebugExit(g_allocator.DeviceFree(d_sum));
    if (d_temp_storage) CubDebugExit(g_allocator.DeviceFree(d_temp_storage));
    
    return 0;
}