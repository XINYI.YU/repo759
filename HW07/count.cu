#include "count.cuh"
#include <thrust/sort.h>
#include <thrust/unique.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>

void count(const thrust::device_vector<int>& d_in,
           thrust::device_vector<int>& values,
           thrust::device_vector<int>& counts) {
    // Sort the input array
    thrust::device_vector<int> sorted_in = d_in;
    thrust::sort(sorted_in.begin(), sorted_in.end());

    // Create arrays for values and counts
    thrust::device_vector<int> unique_values(sorted_in.size());
    thrust::device_vector<int> unique_counts(sorted_in.size());

    thrust::pair<thrust::device_vector<int>::iterator, thrust::device_vector<int>::iterator> new_end;

    // Use thrust::reduce_by_key to count the unique values and their occurrences
    new_end = thrust::reduce_by_key(sorted_in.begin(), sorted_in.end(), thrust::constant_iterator<int>(1),
                                    unique_values.begin(), unique_counts.begin());

    int num_unique = new_end.first - unique_values.begin();

    // Resize the values and counts arrays to match the number of unique values
    values.resize(num_unique);
    counts.resize(num_unique);

    // Copy the unique values and counts to the output arrays
    thrust::copy(unique_values.begin(), new_end.first, values.begin());
    thrust::copy(unique_counts.begin(), new_end.second, counts.begin());
}

