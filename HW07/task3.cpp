#include <iostream>
#include <omp.h>

long long factorial(int n) {
    if (n <= 1) {
        return 1;
    } else {
        long long result = 1; // Use long long to handle large factorials
        for (int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}

int main()
{
#pragma omp parallel
    {
    int myId = omp_get_thread_num();
    int nThreads = omp_get_num_threads();
    std::cout << "I am thread " << myId << " out of " << nThreads << std::endl;
    long long fact1 = factorial(myId+1);
    long long fact2 = factorial(myId+5);
    std::cout << "!" << myId+1 << " = " << fact1 << std::endl;
    std::cout << "!" << myId+5 << " = " << fact2 << std::endl;
    }

}
