#include <omp.h>
#include <iostream>
#include <cstdio>
#include <random>
#include <chrono>
#include "convolution.h"
using std::chrono::high_resolution_clock;
using std::chrono::duration;
using std::chrono::duration_cast;

int main(int argc, char *argv[]){

    std::size_t n = std::atoi(argv[1]);
    std::size_t m = 3;
    std::size_t t = std::atoi(argv[2]);

    float *image = new float[n*n];
    float *mask = new float[m*m];
    float *output = new float[n*n];

    unsigned seed = high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine randGen(seed);
    std::uniform_real_distribution<double> dist1(-10., 10.);
    std::uniform_real_distribution<double> dist2(-1., 1.);
    for(size_t i=0; i<n*n; i++)
        image[i] = dist1(randGen);
    for(size_t i=0; i<m*m; i++)
        mask[i] = dist2(randGen);

    omp_set_num_threads(t);
    high_resolution_clock::time_point start = high_resolution_clock::now();
    convolve(image, output, n, mask, m);
    high_resolution_clock::time_point end = high_resolution_clock::now();
    duration<double, std::milli> duration_millisec = duration_cast< duration<double, std::milli> >(end-start);

    std::cout << output[0] << "\n";
    std::cout << output[n*n-1] << "\n";
    std::cout << duration_millisec.count() << "\n";

    delete[] image;
    delete[] mask;
    delete[] output;

} 
