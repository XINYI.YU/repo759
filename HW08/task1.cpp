#include <iostream>
#include <cstdio>
#include <random>
#include <chrono>
#include "matmul.h"
using std::chrono::high_resolution_clock;
using std::chrono::duration;
using std::chrono::duration_cast;

int main(int argc, char* argv[]){

    const size_t n = std::atoi(argv[1]);
    const size_t t = std::atoi(argv[2]);

    float *A = new float[n*n];
    float *B = new float[n*n];

    unsigned seed = high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine randGen(seed);
    std::uniform_real_distribution<float> dist(-1., 1.);
    for(size_t i=0; i<n*n; i++){
        A[i] = dist(randGen);
        B[i] = dist(randGen);
    }

    omp_set_num_threads(t);
    float *C = new float[n*n];
    high_resolution_clock::time_point start = high_resolution_clock::now();
    mmul(A, B, C, n);
    high_resolution_clock::time_point end = high_resolution_clock::now();
    duration<double, std::milli> duration_millisec = duration_cast< duration<double, std::milli> >(end-start);
    std::cout << C[0] << "\n";
    std::cout << C[n*n-1] << "\n";
    std::cout << duration_millisec.count() << "\n";

    delete[] A;
    delete[] B;
    delete[] C;

}

