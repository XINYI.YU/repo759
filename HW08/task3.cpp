#include <omp.h>
#include <iostream>
#include <random>
#include <chrono>
#include "msort.h"
using std::chrono::high_resolution_clock;
using std::chrono::duration;
using std::chrono::duration_cast;

int main(int argc, char* argv[]) {
    size_t n = std::atoi(argv[1]);
    size_t t = std::atoi(argv[2]);
    size_t ts = std::atoi(argv[3]);

    int* arr = new int[n];
    unsigned seed = high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine randGen(seed);
    std::uniform_int_distribution<int> dist(-1000, 1000);
    for(size_t i=0; i<n; i++)
        arr[i] = dist(randGen);


    omp_set_num_threads(t);

    high_resolution_clock::time_point start = high_resolution_clock::now();
    msort(arr, n, ts);
    high_resolution_clock::time_point end = high_resolution_clock::now();
    duration<double, std::milli> duration_millisec = duration_cast< duration<double, std::milli> >(end-start);

    std::cout<< arr[0] << "\n";
    std::cout<< arr[n-1] << "\n";
    std::cout<< duration_millisec.count() << "\n";

    delete []arr;

}
