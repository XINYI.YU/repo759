#include "time.h"
#include <stdlib.h>
#include <iostream>
#include <cuda.h>
#include "stencil.cuh"

using std::cout;

float* generateRandomFloats(unsigned int n, float upper, float lower, int seed) {
    float* a = new float[sizeof(float)*n];    
    srand((int)time(0)+seed);
    float mag = upper - lower;
    for (unsigned int i=0; i<n; i++)  
    {
        a[i]=lower+1.0*rand()/RAND_MAX*mag;
    }
    return a;
}

int main(int argc, char* argv[]) {
    if (argc != 4) 
    {
        std::cerr << "Usage: ./task2 n R threads_per_block" << std::endl;
        return 1;
    }

    unsigned int n = atoi(argv[1]);
    unsigned int R = atoi(argv[2]);
    unsigned int threads_per_block = atoi(argv[3]);
    
    const unsigned int mask_size = 2 * R + 1;

    // Create, allocate and initialize image, mask, output on host
    float *h_image, *h_mask, *h_output;
    h_image = (float*)malloc(sizeof(float) * n);
    h_mask = (float*)malloc(sizeof(float) * mask_size);
    h_output = (float*)malloc(sizeof(float) * n);

    h_image = generateRandomFloats(n, 1.0, -1.0, 0);
    h_mask = generateRandomFloats(mask_size, 1.0, -1.0, 5);
    h_output = new float[n];

    // Create, allocate and initialize image, mask, output on device
    float *d_image, *d_output, *d_mask;
    cudaMalloc((void**)&d_image, sizeof(float)*n);
    cudaMalloc((void**)&d_output, sizeof(float)*n);
    cudaMalloc((void**)&d_mask, sizeof(float)*(mask_size));

    // Copu image and mask from Host to Device
    cudaMemcpy(d_image, h_image, sizeof(float)*n, cudaMemcpyHostToDevice);
    cudaMemcpy(d_mask, h_mask, sizeof(float)*(mask_size), cudaMemcpyHostToDevice);

    // Cuda Timer
    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start);
    stencil(d_image, d_mask, d_output, n, R, threads_per_block);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    // Copy the Output back from device to host
    cudaMemcpy(h_output, d_output, sizeof(float)*n, cudaMemcpyDeviceToHost);

    // calculate the time
    float ms;
    cudaEventElapsedTime(&ms, start, stop);

    cout << h_output[n-1] << "\n";
    cout << ms << "\n";

    // Free the variables on the Device
    cudaFree(d_image);
    cudaFree(d_mask);
    cudaFree(d_output);

    // Free the variables on the Host
    free(h_image);
    free(h_mask);
    free(h_output);
}