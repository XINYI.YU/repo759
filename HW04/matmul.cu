#include "matmul.cuh"

__global__ void matmul_kernel(const float* A, const float* B, float* C, size_t n){
    int index = blockIdx.x * blockDim.x + threadIdx.x;

    if (index < n) {
        float sum = 0.0f;
        for (int k = 0; k < n; ++k) {
            float a = A[index * n + k];
            float b = B[k * n + index];
            sum += a * b;
        }
        C[index * n + index] = sum;
    }
}


void matmul(const float* A, const float* B, float* C, size_t n, unsigned int threads_per_block) {
    float *d_A, *d_B, *d_C;
    cudaMalloc((void**)&d_A, sizeof(float) * n * n);
    cudaMalloc((void**)&d_B, sizeof(float) * n * n);
    cudaMalloc((void**)&d_C, sizeof(float) * n * n);

    cudaMemcpy(d_A, A, sizeof(float) * n * n, cudaMemcpyHostToDevice);
    cudaMemcpy(d_B, B, sizeof(float) * n * n, cudaMemcpyHostToDevice);

    size_t blockSize = 512;
    int numBlock = (n+blockSize-1)/blockSize;

    matmul_kernel<<<numBlock, threads_per_block>>>(d_A, d_B, d_C, n); 
    cudaMemcpy(C, d_C, sizeof(float) * n * n, cudaMemcpyDeviceToHost);
    
    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);

}