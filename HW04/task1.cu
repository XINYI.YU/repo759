#include <cuda.h>
#include <iostream>
#include <cstdio>
#include <random>
#include <chrono>
#include "matmul.cuh"
using std::chrono::high_resolution_clock;

#define BLOCK_SIZE 512

int main(int argc, char *argv[]){

    int n = std::atoi(argv[1]); 
    int threads_per_block = std::atoi(argv[2]);

    int numBlock = (n+BLOCK_SIZE-1)/BLOCK_SIZE;

    int size = sizeof(float);

    // Allocate host memory for matrices A, B, C
    float *h_A, *h_B, *h_C;
    h_A = (float *)malloc(size * n * n);
    h_B = (float *)malloc(size * n * n);
    h_C = (float *)malloc(size * n * n);

    // Initialize matrices A and B with random values between -1 and 1
    unsigned seed = high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine randGen(seed);
    std::uniform_real_distribution<float> distA(-1, 1);
    for(int i=0; i<n*n; i++){
        h_A[i] = distA(randGen);
        h_B[i] = distA(randGen);
    }

    // Allocate device memory for matrices A, B, C
    // float *d_A, *d_B, *d_C;
    // cudaMalloc((void **)&d_A, size *n *n);
    // cudaMalloc((void **)&d_B, size *n *n);
    // cudaMalloc((void **)&d_C, size *n *n);
    
    // // CPU ==> GPU
    // cudaMemcpy(d_A, h_A, size * n * n, cudaMemcpyHostToDevice);
    // cudaMemcpy(d_B, h_B, size * n * n, cudaMemcpyHostToDevice);

    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);

    // Invoke Kernel on GPU
    // matmul_kernel<<<numBlock, BLOCK_SIZE>>>(d_A, d_B, d_C, n);
    matmul(h_A, h_B, h_C, n, threads_per_block);
    
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float ms;
    cudaEventElapsedTime(&ms, start, stop);

    // GPU ==> CPU
    // cudaMemcpy(h_C, d_C, size * n * n, cudaMemcpyDeviceToHost);

    std::cout <<  h_C[n * n - 1] << std::endl;
    std::cout <<  ms <<  std::endl;

    free(h_A);
    free(h_B);
    free(h_C);
    // cudaFree(d_A);
    // cudaFree(d_B);
    // cudaFree(d_C);
}