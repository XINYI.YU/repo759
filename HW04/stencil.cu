#include "stencil.cuh"

__global__ void stencil_kernel(const float* image, const float* mask, float* output, unsigned int n, unsigned int R) {
    
    // Allocate Shared Memory
    extern __shared__ float shared_memory[];
    
    // Create index and variables starting position
    unsigned int tid = blockDim.x*blockIdx.x+threadIdx.x;
    unsigned int image_start_position = 2*R+1;
    unsigned int output_start_position = blockDim.x+4*R+1;

    // Copy the Mask to shared Memory
    if (threadIdx.x<(2*R+1))    
    {
        shared_memory[threadIdx.x] = mask[threadIdx.x];
    }

    // Copy the Image to Shared Memory
    if (threadIdx.x==0) 
    {
        for (unsigned int i=0; i<R; i++) 
        {
            if ((tid+i)<R)  
            {
                shared_memory[image_start_position+i]=1.0;
            }
            else 
            {
                shared_memory[image_start_position+i] = image[tid-R+i];
            }
        }
    }

    shared_memory[image_start_position+R+threadIdx.x] = image[tid];

    if (threadIdx.x==(blockDim.x-1)) 
    {
        for (unsigned int j=0; j<R; j++) 
        {
            if ((tid+1+j)>(n-1)) 
            {
                shared_memory[image_start_position+R+blockDim.x+j]=1.0;
            }
            else 
            {
                shared_memory[image_start_position+R+blockDim.x+j] = image[tid+1+j];
            }
        }
    }
    __syncthreads();

    // Calculate Convolution Process
    shared_memory[output_start_position+threadIdx.x] = 0.0;

    for (unsigned int k=0; k<(2*R+1); k++)
    {
        shared_memory[output_start_position+threadIdx.x] += shared_memory[image_start_position+threadIdx.x+k]*shared_memory[k];
    }

    __syncthreads();

    // copy result data back to output array
    output[tid] = shared_memory[output_start_position+threadIdx.x];
    __syncthreads();
    
}

__host__ void stencil(const float* image,
                      const float* mask,
                      float* output,
                      unsigned int n,
                      unsigned int R,
                      unsigned int threads_per_block) {
    // shared memory size
    unsigned int shared_memory_size = 4*R + 1 + 2 * threads_per_block;
    // number of blocks
    unsigned int numBlock = (n+threads_per_block-1)/threads_per_block;

    // invoke the kernel
    stencil_kernel<<<numBlock, threads_per_block, shared_memory_size*sizeof(float)>>>(image, mask, output, n, R);

    //synchronize
    cudaDeviceSynchronize();
}