#include <cuda.h>
#include <iostream>
#include <cstdio>
#include <random>
#include <chrono>
#include "vscale.cuh"
using std::chrono::high_resolution_clock;


int main(int argc, char *argv[]){

    int n = std::atoi(argv[1]);
    int blockSize = 512;
    int numBlock = (n+blockSize-1)/blockSize;

    float *a = new float[n];
    float *b = new float[n];
    unsigned seed = high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine randGen(seed);
    std::uniform_real_distribution<float> distA(-10, 10);
    std::uniform_real_distribution<float> distB(0, 1);
    for(int i=0; i<n; i++){
        a[i] = distA(randGen);
        b[i] = distB(randGen);
    }

    // Allocate Memory on GPU 
    float *ad;
    float *bd;

    cudaMalloc((void **)&ad, sizeof(float)*n);
    cudaMalloc((void **)&bd, sizeof(float)*n);


    // CPU ==> GPU
    cudaMemcpy(ad, a, sizeof(float)*n, cudaMemcpyHostToDevice);
    cudaMemcpy(bd, b, sizeof(float)*n, cudaMemcpyHostToDevice);

    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);
    // Invoke Kernel on GPU
    vscale<<<numBlock, blockSize>>>(ad, bd, n);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float ms;
    cudaEventElapsedTime(&ms, start, stop);

    // GPU ==> CPU
    cudaMemcpy(b, bd, sizeof(float)*n, cudaMemcpyDeviceToHost);

    // Fee memory on GPU
    cudaFree(ad);
    cudaFree(bd);

    std::cout << ms << "\n";
    std::cout << b[0] << "\n";
    std::cout << b[n-1] << "\n";

    delete[] a;
    delete[] b;

}
