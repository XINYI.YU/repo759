#include <cuda.h>
#include <cstdio>


__global__ void factorial8(){

    int tid = threadIdx.x;

    int factorial = 1;

    for(int i=1; i<=tid+1; i++)
        factorial *= i;
    
    printf("%d!=%d\n", tid+1, factorial);
    

}




int main(){

    factorial8<<<1, 8>>>();

    cudaDeviceSynchronize();

}

