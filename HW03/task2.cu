#include <cuda.h>
#include <cstdio>
#include <random>
#include <chrono>
using std::chrono::high_resolution_clock;


__global__ void kernel(int *dA, int a){

    int tid = blockIdx.x*blockDim.x + threadIdx.x;
    int x = threadIdx.x;
    int y = blockIdx.x;

    dA[tid] = a*x+y;

}


int main(){
    
    unsigned seed = high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine randGen(seed);
    std::uniform_int_distribution<int> dist(1, 10);
    int a = dist(randGen);

    int n = 16;

    int *dA;
    cudaMalloc((void **)&dA, n*sizeof(int));
    kernel<<<2, 8>>>(dA, a);

    int hA[n];
    cudaMemcpy(hA, dA, n*sizeof(int), cudaMemcpyDeviceToHost);

    cudaFree(dA);

    for(int i=0; i<n; i++)
        printf("%d ", hA[i]);
    printf("\n");


}


