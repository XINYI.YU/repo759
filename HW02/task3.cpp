#include <iostream>
#include <cstdio>
#include <random>
#include <chrono>
#include "matmul.h"
using std::chrono::high_resolution_clock;
using std::chrono::duration;
using std::chrono::duration_cast;


int main(){

    unsigned int n = 1024;
    std::cout << n << "\n";

    double *A = new double[n*n];
    double *B = new double[n*n];

    unsigned seed = high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine randGen(seed);
    std::uniform_real_distribution<double> dist(-1., 1.);
    for(int i=0; i<n*n; i++){
        A[i] = dist(randGen);
        B[i] = dist(randGen);
    }
    
    //matmul1
    double *C1 = new double[n*n];
    high_resolution_clock::time_point start = high_resolution_clock::now();
    mmul1(A, B, C1, n);
    high_resolution_clock::time_point end = high_resolution_clock::now();
    duration<double, std::milli> duration_millisec = duration_cast< duration<double, std::milli> >(end-start);
    std::cout << duration_millisec.count() << "\n";
    std::cout << C1[n*n-1] << "\n";

    //matmul2
    double *C2 = new double[n*n];
    start = high_resolution_clock::now();
    mmul2(A, B, C2, n);
    end = high_resolution_clock::now();
    duration_millisec = duration_cast< duration<double, std::milli> >(end-start);
    std::cout << duration_millisec.count() << "\n";
    std::cout << C2[n*n-1] << "\n";

    //matmul3
    double *C3 = new double[n*n];
    start = high_resolution_clock::now();
    mmul3(A, B, C3, n);
    end = high_resolution_clock::now();
    duration_millisec = duration_cast< duration<double, std::milli> >(end-start);
    std::cout << duration_millisec.count() << "\n";
    std::cout << C3[n*n-1] << "\n";

    //matmul4
    std::vector<double> vecA(A, A+n*n);
    std::vector<double> vecB(B, B+n*n);
    double *C4 = new double[n*n];
    start = high_resolution_clock::now();
    mmul4(vecA, vecB, C4, n);
    end = high_resolution_clock::now();
    duration_millisec = duration_cast< duration<double, std::milli> >(end-start);
    std::cout << duration_millisec.count() << "\n";
    std::cout << C4[n*n-1] << "\n";

    delete[] A;
    delete[] B;
    delete[] C1;
    delete[] C2;
    delete[] C3;
    delete[] C4;

}


