#include "scan.h"

void scan(const float *arr, float *output, std::size_t n){

    float accumulator = 0.;
    for(size_t i=0; i<n; i++){
        accumulator += arr[i];
        output[i] = accumulator;
    }
}

