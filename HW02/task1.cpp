#include <iostream>
#include <cstdio>
#include <cstdlib> 
#include <random>
#include <chrono>
#include <ratio>
#include "scan.h"
using std::chrono::high_resolution_clock;
using std::chrono::duration;
using std::chrono::duration_cast;


int main(int argc, char *argv[]){

    size_t n = std::atoi(argv[1]);
    float *arr = new float[n];
    float *output = new float[n];
    std::default_random_engine randGen;
//    std::uniform_real_distribution<double> dist(-1., 1.);
//    for(size_t i=0; i<n; i++)
//        arr[i] = dist(randGen);

    // Seed the random number generator
    std::srand(static_cast<unsigned int>(std::time(nullptr)));

    // Create an array of n random float numbers between -1.0 and 1.0
//    float *inputArray = new float[n];
    for (std::size_t i = 0; i < n; ++i) {
        arr[i] = static_cast<float>(std::rand()) / RAND_MAX * 2.0 - 1.0;
    }
    
    high_resolution_clock::time_point start = high_resolution_clock::now();
    scan(arr, output, n);
    high_resolution_clock::time_point end = high_resolution_clock::now();
    duration<double, std::milli> duration_millisec = duration_cast< duration<double, std::milli> >(end-start);

    std::cout <<"Time: " << duration_millisec.count() << ", First Output: " << output[0] << ", Last Output: " << output[n-1] << "\n";

    delete[] arr;
    delete[] output;

}


