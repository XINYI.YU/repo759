#include "mmul.h"
#include <iostream>
#include <random>
#include <chrono>
using std::chrono::high_resolution_clock;

int main(int argc, char *argv[]){

    size_t N = std::atoi(argv[1]);
    size_t N_Tests = std::atoi(argv[2]);
    float *A, *B, *C;
    cudaMallocManaged(&A, sizeof(float)*N*N);
    cudaMallocManaged(&B, sizeof(float)*N*N);
    cudaMallocManaged(&C, sizeof(float)*N*N);

    unsigned seed = high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine ranGen(seed);
    std::uniform_real_distribution<float> uniformDist(-1, 1);

    for(size_t i=0; i<N*N; i++){
        A[i] = uniformDist(ranGen);
        B[i] = uniformDist(ranGen);
    }

    cublasHandle_t handle;
    cublasCreate(&handle);


    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);
    for(size_t i=0; i<N_Tests; i++){
        mmul(handle, A, B, C, N);
    }
    cudaDeviceSynchronize()
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float ms;
    cudaEventElapsedTime(&ms, start, stop);


    cublasDestroy(handle);

    std::cout<< ms/N_Tests << "\n";
    
    cudaFree(A);
    cudaFree(B);
    cudaFree(C);

}


