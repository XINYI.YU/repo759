#include <cuda.h>
#include <iostream>
#include <cstdio>
#include <random>
#include <chrono>
#include "scan.cuh"
using std::chrono::high_resolution_clock;

int main(int argc, char *argv[]){

    int n = std::atoi(argv[1]);
 	int threadsPerBlock = std::atoi(argv[2]);

    float *arr, *res;
    cudaMallocManaged(&arr, sizeof(float)*n);
    cudaMallocManaged(&res, sizeof(float)*n);

    unsigned seed = high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine randGen(seed);
    std::uniform_real_distribution<float> uniformDist(-1, 1);
    for(int i=0; i<n; i++)
        arr[i] = uniformDist(randGen);

  	cudaEvent_t start;
  	cudaEvent_t stop;
  	cudaEventCreate(&start);
  	cudaEventCreate(&stop);
  	cudaEventRecord(start);
    scan(arr, res, n, threadsPerBlock);
  	cudaEventRecord(stop);
  	cudaEventSynchronize(stop);
	float ms;
	cudaEventElapsedTime(&ms, start, stop);

	printf("%f\n%f\n", res[n-1], ms);

    cudaFree(arr);
    cudaFree(res);



}








