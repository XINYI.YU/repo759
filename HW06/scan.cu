#include <cuda.h>
#include "scan.cuh"

__global__ void hs(const float* input, float* output, float* temp_arr, unsigned int n, bool isFlag) {
    extern volatile __shared__ float temp[];

    int thid_in_block = threadIdx.x; 
    int thid_global = blockIdx.x*blockDim.x+threadIdx.x;
    int blockSize = blockDim.x;

    if(thid_global < n)
        temp[thid_in_block] = input[thid_global];

    __syncthreads();

    // hs for the block
    int pin=1, pout=0;
    for (int offset=1; offset<blockSize; offset=offset<<1) {
        pin = 1 - pin;
        pout = 1- pin;

        if (thid_in_block >= offset)
            temp[pout*blockSize+thid_in_block] = temp[pin*blockSize+thid_in_block] + temp[pin*blockSize+thid_in_block - offset];
	    else
            temp[pout*blockSize+thid_in_block] = temp[pin*blockSize+thid_in_block];
        __syncthreads(); 
    }

    // write 
    if (thid_global<n)
        output[thid_global] = temp[pout*blockSize+thid_in_block];

    // write the last element of the block
    if ( (isFlag) && (thid_in_block==(blockSize-1)) )
        temp_arr[thid_in_block] = temp[pout*blockSize+thid_in_block];

}


__global__ void add_Block(float* output, float* temp_arr, unsigned int n) {

    __shared__ float temp_var;

    if( (threadIdx.x==0) && (blockIdx.x==0) )
        temp_var = 0.0;
    else if( (threadIdx.x==0) && (blockIdx.x>0) )
        temp_var = temp_arr[blockIdx.x-1];

    if(blockIdx.x*blockDim.x+threadIdx.x<n)  
        output[blockIdx.x*blockDim.x+threadIdx.x] += temp_var;

}




__host__ void scan(const float* input, float* output, unsigned int n, unsigned int threads_per_block) {

    int numBlock = (n+threads_per_block-1)/threads_per_block;

    float* temp_arr; 
    cudaMalloc((void**)&temp_arr, sizeof(float)*numBlock);

    hs<<<numBlock, threads_per_block, 2*threads_per_block*sizeof(float)>>>(input, output, temp_arr, n, true);
    hs<<<1, numBlock, 2*numBlock*sizeof(float)>>>(temp_arr, temp_arr, temp_arr, n, false);
    add_Block<<<numBlock,threads_per_block,sizeof(float)>>>(output, temp_arr, n);

    cudaDeviceSynchronize();

    cudaFree(temp_arr);
}

 
