#include "mmul.h"


void mmul(cublasHandle_t handle, const float* A, const float* B, float* C, int n){

    // Define scalars for the matrix operation
    float alpha = 1.0f;
    float beta = 1.0f;

    // Call the cuBLAS SGEMM function for single precision general matrix multiply
    cublasSgemm(handle,
                CUBLAS_OP_N, CUBLAS_OP_N,
                n, n, n,
                &alpha,
                A, n,
                B, n,
                &beta,
                C, n);
    

}













