#!/usr/bin/env zsh

#SBATCH -p instruction         # Partition selection
#SBATCH -t 0-00:05:00          # Run time
#SBATCH -J FirstSlurm          # Job name

#SBATCH -o FirstSlurm.out -e FirstSlurm.err 

#SBATCH -c 2                   # Specify number of CPU needed 
#SBATCH --mem=2

# Other commands must follow all #SBATCH directives...
hostname

# ---------------------------------------------------
