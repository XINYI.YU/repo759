#!/bin/bash

echo "Serial Jacobi Solver:"
nvcc -o serialJacobi test_serialJacobi.cu inOutFuncs.cu serialFuncs.cu
echo "Compile Finished!"

echo "Serial LU Decomposition:"  
nvcc -o serialLU test_serialLUdecomp.cu inOutFuncs.cu serialFuncs.cu
echo "Compile Finished!"

echo "Cuda LU Decomposition:"
nvcc test_cudaLUdecomp.cu inOutFuncs.cu serialFuncs.cu cudaFuncs.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -lcublas -lcusolver -arch sm_75 -o cudaLU
echo "Compile Finished!"

echo "Cuda Jacobi Solver:"
nvcc test_cudaJacobi.cu inOutFuncs.cu serialFuncs.cu cudaFuncs.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -lcublas -lcusolver -arch sm_75 -o cudaJacobi
echo "Compile Finished!"

echo "Cublas LU Decomposition:"
nvcc test_cublasLUDecomp.cu inOutFuncs.cu serialFuncs.cu cudaFuncs.cu cublasFuncs.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -lcublas -lcusolver -arch sm_75 -o cublasLU
echo "Compile Finished!"

echo "Cublas Jacobi Solver:"
nvcc test_cublasJacobi.cu inOutFuncs.cu serialFuncs.cu cudaFuncs.cu cublasFuncs.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -lcublas -lcusolver -arch sm_75 -o cublasJacobi
echo "Compile Finished!"

echo "Time Comparison"
nvcc test_timeComparison.cu inOutFuncs.cu serialFuncs.cu cudaFuncs.cu cublasFuncs.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -lcublas -lcusolver -arch sm_75 -o timeComparison
echo "Compile Finished!"