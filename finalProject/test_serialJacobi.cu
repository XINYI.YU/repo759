#include <string>
#include <iostream>
#include "inOutFuncs.cuh"
#include "serialFuncs.cuh"


int main(int argc, char* argv[]) {
    matrixInfo* a = readMatrixFromText("./matrixData/A3.txt");
    std::cout << "Finish Reading serial A" << std::endl; 

    matrixInfo* b = readMatrixFromText("./matrixData/b3.txt");
    std::cout << "Finish Reading serial b" << std::endl; 

    std::cout << "A matrix" << std::endl;
    printMatrix(a);

    std::cout << "b matrix" << std::endl;
    printMatrix(b);
    
    matrixInfo* sol1 = serialJacobiSolve(a,b, 100, 1e-5);

    std::cout << "Soluntion Matrix using Serial Jacobian Solver" << std::endl;
    printMatrix(sol1);

    delete [](sol1->matrix);
    delete sol1; 

    delete [](a->matrix);
    delete a;
    delete [](b->matrix);
    delete b;
    return 0;
}