This is the beginning of the final project.

Compile Command: 

    1. Serial Jacobi Solver:     nvcc -o serialJacobi test_serialJacobi.cu inOutFuncs.cu serialFuncs.cu

    2. Serial LU Decomposition:  nvcc -o serialLU test_serialLUdecom.cu inOutFuncs.cu serialFuncs.cu

    3. Cuda LU Decomposition:    nvcc test_cudaLUdecomp.cu inOutFuncs.cu serialFuncs.cu cudaFuncs.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -lcublas -lcusolver -arch sm_75 -o cudaLU
    
    4. Cuda Jacobi Solver:       nvcc test_cudaJacobi.cu inOutFuncs.cu serialFuncs.cu cudaFuncs.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -lcublas -lcusolver -arch sm_75 -o cudaJacobi
    
    5. Cublas LU Decomposition:  nvcc test_cublasLUDecomp.cu inOutFuncs.cu serialFuncs.cu cudaFuncs.cu cublasFuncs.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -lcublas -lcusolver -arch sm_75 -o cublasL

    6. Cublas Jacobi Solver:     nvcc test_cublasLUDecomp.cu inOutFuncs.cu serialFuncs.cu cudaFuncs.cu cublasFuncs.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -lcublas -lcusolver -arch sm_75 -o cublasL    

    7. Time Comparison Part:     nvcc test_timeComparison.cu inOutFuncs.cu serialFuncs.cu cudaFuncs.cu cublasFuncs.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -lcublas -lcusolver -arch sm_75 -o timeComparison
