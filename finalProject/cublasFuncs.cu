#include <string>
#include <stdio.h>

#include <cublas_v2.h>
#include <cusolverDn.h>

#include "inOutFuncs.cuh"
#include "serialFuncs.cuh"
#include "cudaFuncs.cuh"
#include "cublasFuncs.cuh"

__global__ void JacobiDecompose(double* A, double* D, unsigned int n) {
    unsigned int my_global_index = blockIdx.x*blockDim.x + threadIdx.x;
    if (my_global_index<(n*n)) {
        if (my_global_index%(n+1)==0) {
            D[my_global_index/(n+1)] = A[my_global_index];
            A[my_global_index] = 0.0;
        }
        else    A[my_global_index] = (-1.0)*A[my_global_index];
    }
}

__global__ void JacobiUpdate(double* b, double* x, double* D, double* temp, unsigned int n) {
    unsigned int my_global_index = blockIdx.x*blockDim.x + threadIdx.x;
    if (my_global_index<n) {
        x[my_global_index] = (b[my_global_index]+temp[my_global_index]) / D[my_global_index];
    }
}

__global__ void cudaTranspose(double* matrix, unsigned int n) {
    unsigned int my_global_index = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int my_global_row = my_global_index/n;
    unsigned int my_global_col = my_global_index%n;

    if (my_global_index<(n*n)&&(my_global_col>my_global_row)) {
        double swap = matrix[my_global_row*n+my_global_col];
        matrix[my_global_row*n+my_global_col] = matrix[my_global_col*n+my_global_row];
        matrix[my_global_col*n+my_global_row] = swap;
    }
}

matrixInfo* cublasJacobiSolve(matrixInfo* A_mat, matrixInfo* b_mat, unsigned int max_iter, double tol, unsigned int threads_per_block) {
    unsigned int iter = 0;
    double error = 100.0;
    
    double* A = A_mat->matrix;
    double* b = b_mat->matrix;
    unsigned int num_row = A_mat->num_row;

    // allocate matrix in device, and copy data from host to device
    double* dA; cudaMalloc(&dA, num_row*num_row*sizeof(double));
    double* db; cudaMalloc(&db, num_row*sizeof(double));
    double* dD; cudaMalloc(&dD, num_row*sizeof(double));
    double* dx; cudaMalloc(&dx, num_row*sizeof(double));
    double* dtemp; cudaMallocHost(&dtemp, num_row*sizeof(double));
    cudaMemcpy(dA,A,num_row*num_row*sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(db,b,num_row*sizeof(double),cudaMemcpyHostToDevice);

    // decompose A to diaganol vector D and negative A
    unsigned int numBlock = (num_row*num_row+threads_per_block-1)/threads_per_block;
    JacobiDecompose<<<numBlock,threads_per_block>>>(dA, dD, num_row);

    // prepare cuBLAS
    cublasHandle_t handle;
    cublasCreate(&handle);
    const double alpha = 1.0;
    const double beta = 0.0;

    // multiply temp = A*x
    numBlock = (num_row+threads_per_block-1)/threads_per_block;
    while ((iter<max_iter)&&(error>tol)) {
        cublasStatus_t stat = cublasDgemm(handle,
                                          CUBLAS_OP_T, CUBLAS_OP_N,
                                          num_row, 1, num_row,
                                          &alpha,
                                          dA, num_row,
                                          dx, num_row,
                                          &beta,
                                          dtemp, num_row);
        JacobiUpdate<<<numBlock,threads_per_block>>>(db, dx, dD, dtemp, num_row);
        iter++;
    }

    // create return handle
    matrixInfo* ret = new matrixInfo;
    ret->num_row = num_row;
    ret->num_col = 1;
    ret->matrix  = new double[num_row];

    cudaMemcpy(ret->matrix,dx,num_row*sizeof(double),cudaMemcpyDeviceToHost);

    // release the device memory
    cudaFree(dA);
    cudaFree(db);
    cudaFree(dD);
    cudaFree(dx);
    cudaFree(dtemp);
    cublasDestroy(handle);

    return ret;
}


matrixInfo* cublasLUDecomposition(matrixInfo* A_mat, matrixInfo* b_mat, unsigned int threads_per_block) {
    // prepare the cublas
    cusolverDnHandle_t handle;
    cusolverDnCreate(&handle);

    double* A = A_mat->matrix;
    double* b = b_mat->matrix;
    unsigned int num_row = A_mat->num_row;

    // allocate matrix in device, and copy data from host to device
    double* dA; cudaMalloc(&dA, num_row*num_row*sizeof(double));
    double* db; cudaMalloc(&db, num_row*sizeof(double));
    int* dtemp; cudaMalloc(&dtemp, sizeof(int));
    cudaMemcpy(dA,A,num_row*num_row*sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(db,b,num_row*sizeof(double),cudaMemcpyHostToDevice);

    // transpose
    unsigned int numBlock = (num_row*num_row+threads_per_block-1)/threads_per_block;
    cudaTranspose<<<numBlock,threads_per_block>>>(dA, num_row);

    // calculate buffer size for cuSOLVER LU factorization
    int Lwork;
    cusolverDnDgetrf_bufferSize(handle, num_row, num_row, dA, num_row, &Lwork);
    double* workArray;  cudaMalloc((void**)&workArray, Lwork*sizeof(double));

    cusolverDnDgetrf(handle, num_row, num_row, dA, num_row, workArray, NULL, dtemp);
    cusolverDnDgetrs(handle, CUBLAS_OP_N, num_row, 1, dA, num_row, NULL, db, num_row, dtemp);
    
    // create calculation result
    matrixInfo* ret = new matrixInfo;
    ret->num_row = num_row;
    ret->num_col = 1;
    ret->matrix  = new double[num_row];
    cudaMemcpy(ret->matrix,db,num_row*sizeof(double),cudaMemcpyDeviceToHost);

    // free the device memorry
    cudaFree(dA);
    cudaFree(db);
    cudaFree(dtemp);
    cusolverDnDestroy(handle);

    return ret;
}