#include <string>
#include <iostream>
#include "inOutFuncs.cuh"
#include "serialFuncs.cuh"
#include "cudaFuncs.cuh"

int main(int argc, char* argv[]) {
    matrixInfo* a = readMatrixFromText("./matrixData/A3.txt");
    std::cout << "Finish Reading serial A" << std::endl; 

    matrixInfo* b = readMatrixFromText("./matrixData/b3.txt");
    std::cout << "Finish Reading serial b" << std::endl; 

    std::cout << "A matrix" << std::endl;
    printMatrix(a);

    std::cout << "b matrix" << std::endl;
    printMatrix(b);
    
    cudaLUDecomposition(a, 2);
    matrixInfo* sol1 = LUSolve(a,b);
    
    std::cout << "Soluntion Matrix using Raw CUDA LU Decomposition" << std::endl;
    printMatrix(sol1);

    delete [](sol1->matrix);
    delete sol1; 

    delete [](a->matrix);
    delete a;
    delete [](b->matrix);
    delete b;

    return 0;
}