#ifndef CUBLASFUNCS_CUH
#define CUBLASFUNCS_CUH

#include <string>
#include <stdio.h>

#include <cuda.h>
#include <cublas_v2.h>

#include "inOutFuncs.cuh"
#include "serialFuncs.cuh"
#include "cudaFuncs.cuh"

__global__ void JacobiDecompose(double *mat, unsigned int n);

__global__ void JacobiUpdate(double* b, double* x, double* D, double* temp, unsigned int n);

__global__ void cudaTranspose(double* matrix, unsigned int m);

matrixInfo* cublasJacobiSolve(matrixInfo* A, matrixInfo* b, unsigned int max_iter, double tol, unsigned int threads_per_block);

matrixInfo* cublasLUDecomposition(matrixInfo* A, matrixInfo* b_mat, unsigned int threads_per_block);
#endif