#include "serialFuncs.cuh"
#include <iostream>
#include <stdio.h>

void serialLUdecomp(matrixInfo* matrix) {
    unsigned int num_row = matrix->num_row;
    unsigned int num_col = matrix->num_col;
    double* mat = matrix->matrix;

    for (unsigned int m1 = 0; m1 < num_row; m1++) {
        for (unsigned int m2 = m1; m2 < num_col; m2++) {
            for (unsigned int m3 = 0; m3 < m1; m3++) {
                mat[m1*num_row + m2] -= mat[m1*num_row+m3] * mat[m3*num_row + m2];
            }
        }
        if (m1 < (num_row - 1)) {
            for (unsigned int m2 = m1 + 1; m2 < num_col; m2++) {
                for (unsigned int m3 = 0; m3 < m1; m3++) {
                    mat[m2*num_row + m1] -= mat[m2*num_row + m3]*mat[m3*num_row+m1];
                }
                mat[m2*num_row + m1] /= mat[m1*num_row + m1];
            }
        }
    }
}


matrixInfo* LUSolve(matrixInfo* A_mat, matrixInfo* b_mat) {
    unsigned int num_row = A_mat->num_row;
    matrixInfo* solution = new matrixInfo;
    solution->num_row = num_row;
    solution->num_col = 1;
    solution->matrix = new double[num_row];

    double temp_sum;
    double* sol = solution->matrix;
    double* A = A_mat->matrix;
    double* b = b_mat->matrix;

    for (unsigned int m1 = 0; m1 < num_row; m1++) {
        temp_sum = 0.0;
        for (unsigned int m2 = 0; m2 < m1; m2++) {
            temp_sum += A[m1 * num_row + m2]* sol[m2];
        }
        sol[m1] = b[m1] - temp_sum;
    }

    for (unsigned int m1 = 0; m1 < num_row; m1++) {
        unsigned int curr_m1 = num_row - m1 -1;
        temp_sum = 0.0;
        for (unsigned int m2 = curr_m1 + 1; m2 < num_row; m2++) {
            temp_sum += A[curr_m1 * num_row + m2] * sol[m2];
        }
        sol[curr_m1] = (sol[curr_m1] - temp_sum)/A[curr_m1 * num_row + curr_m1];
    }

    return solution;
}

matrixInfo* serialJacobiSolve(matrixInfo* A_mat, matrixInfo* b_mat, unsigned int max_iter, double tol=1e-5) {
    unsigned int iter = 0;
    double error = 100.0;
    
    unsigned int num_row = A_mat->num_row;
    double* A = A_mat->matrix;
    double* b = b_mat->matrix;

    double* temp1 = new double[num_row];
    double* temp2 = new double[num_row];
    double* swap;

    while ((iter<max_iter)&&(error>tol)) {
        for (unsigned int m1=0; m1<num_row; m1++) {
            double sum = 0.0;
            for (unsigned int m2=0; m2<m1; m2++)         sum += A[m1*num_row+m2]*temp1[m2];
            for (unsigned int m2=m1+1; m2<num_row; m2++) sum += A[m1*num_row+m2]*temp1[m2];
            temp2[m1] = (b[m1]-sum)/A[m1*num_row+m1];
        }
        swap = temp1; 
        temp1 = temp2;
        temp2 = swap;
        iter++;
    }

    delete []temp2;
    matrixInfo* solution = new matrixInfo;
    solution->num_row = num_row;
    solution->num_col = 1;
    solution->matrix = temp1;

    return solution;

}