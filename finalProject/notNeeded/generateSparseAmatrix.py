import numpy as np
from scipy.sparse import random, csr_matrix

def generate_sparse_matrix(N, density):
    matrix_A = random(N, N, density=density)
    while np.linalg.norm(matrix_A.toarray(), ord=np.inf) >= 1:
        matrix_A = random(N, N, density=density)
    return matrix_A

def generate_matrix_b(N):
    matrix_b = np.random.rand(N, 1)
    return matrix_b

def save_matrix_to_text(matrix, filename):
    np.savetxt(filename, matrix.toarray())

N = 10  # Change N to the desired size of the matrices
density = 0.2  # Adjust density for the desired sparsity level

# Generate sparse matrix A and matrix b
sparse_matrix_A = generate_sparse_matrix(N, density)
matrix_b = generate_matrix_b(N)

# Convert sparse matrix A to CSR format
sparse_matrix_A_csr = csr_matrix(sparse_matrix_A)

# Save sparse matrix A and matrix b to text files
save_matrix_to_text(sparse_matrix_A_csr, "sparse_matrix_A.txt")
np.savetxt("matrix_b.txt", matrix_b)

print("Sparse matrix saved to sparse_matrix_A.txt and matrix_b.txt")
