#ifndef CUDASPARSEFUNCS_CUH
#define CUDASPARSEFUNCS_CUH

#include <string>
#include <stdio.h>

#include <cuda.h>
#include <cublas_v2.h>

#include "inOutFuncs.cuh"
#include "serialFuncs.cuh"
#include "cudaFuncs.cuh"

__global__ void cudaSparseJacobiKernel(unsigned int* A_row_start, unsigned int* A_col, double* A_val, double* b, double* x1, double* x2, unsigned int num_row, unsigned int num_val);

__host__ matrixInfo* cudaSparseJacobiSolve(sparseMatrixInfo* A, matrixInfo* b, unsigned int max_iter, double tol, unsigned int threads_per_block);

#endif