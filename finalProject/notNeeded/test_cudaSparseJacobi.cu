#include <stdio.h>
#include <string>
#include <iostream>

#include <cublas_v2.h>
#include <cusolverDn.h>

#include "inOutFuncs.cuh"
#include "serialFuncs.cuh"
#include "cudaFuncs.cuh"
#include "cublasFuncs.cuh"
#include "cudaSparseFuncs.cuh"

int main(int argc, char* argv[]) {
    sparseMatrixInfo* a = readMatrixFromText("sparse_matrix_A.txt");
    std::cout << "Finish Reading serial A" << std::endl; 

    matrixInfo* b = readMatrixFromText("matrix_b.txt");
    std::cout << "Finish Reading serial b" << std::endl; 

    std::cout << "A matrix" << std::endl;
    printMatrix(a);

    std::cout << "b matrix" << std::endl;
    printMatrix(b);
    
    matrixInfo* sol1 = cudaSparseJacobiSolve(a, b, 1000, 1e-5, 2);
    
    std::cout << "Soluntion Matrix using Serial Jacobian Solver" << std::endl;
    printMatrix(sol1);
    delete [](sol1->matrix);
    delete sol1; 

    delete [](a->matrix);
    delete a;
    delete [](b->matrix);
    delete b;

    return 0;
}