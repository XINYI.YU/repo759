#include <string>
#include <stdio.h>

#include <cublas_v2.h>
#include <cusolverDn.h>

#include "inOutFuncs.cuh"
#include "serialFuncs.cuh"
#include "cudaFuncs.cuh"
#include "cublasFuncs.cuh"
#include "cudaSparseFuncs.cuh"

__global__ void cudaSparseJacobiKernel(unsigned int* A_row_start, unsigned int* A_col, double* A_val, double* b, double* x1, double* x2, unsigned int num_row, unsigned int num_val) {
    unsigned int index = blockIdx.x*blockDim.x+threadIdx.x;
    unsigned int start_index, end_index, self_index=0;
    
    if (index<num_row) {
        start_index = A_row_start[index];
        
        if (index<(num_row-1))       end_index = A_row_start[index+1];
        else                         end_index = num_val;
        
        double sum = 0.0;
        for (unsigned int m1=start_index; m1<end_index; m1++) {
            unsigned int col_index = A_col[m1];
            if (col_index!=index)   sum += A_val[m1]*x1[col_index];
            else                    self_index = m1;
        }
            
        x2[index] = (b[index]-sum)/A_val[self_index];
    }

    return;
}


__host__ matrixInfo* cudaSparseJacobiSolve(sparseMatrixInfo* A_mat, matrixInfo* b_mat, unsigned int max_iter, double tol, unsigned int threads_per_block) {
    unsigned int iter = 0;
    double error = 100.0;
    
    unsigned int num_row = A_mat->num_row;
    unsigned int num_val = A_mat->num_val;
    unsigned int numBlock = (num_row+threads_per_block-1)/threads_per_block;

    // allocate the device memory
    unsigned int* dA_row_start; cudaMalloc((void**)&dA_row_start, num_row*sizeof(unsigned int));
    unsigned int* dA_col; cudaMalloc((void**)&dA_col, num_val*sizeof(unsigned int));
    double* dA_val; cudaMalloc((void**)&dA_val, num_val*sizeof(double));
    double* db; cudaMalloc((void**)&db, num_row*sizeof(double));
    double* dx1; cudaMalloc((void**)&dx1, num_row*sizeof(double));
    double* dx2; cudaMalloc((void**)&dx2, num_row*sizeof(double));

    // copy input array from host to device
    cudaMemcpy(dA_row_start,A_mat->row_start_index,num_row*sizeof(unsigned int),cudaMemcpyHostToDevice);
    cudaMemcpy(dA_col,A_mat->col_index,num_val*sizeof(unsigned int),cudaMemcpyHostToDevice);
    cudaMemcpy(dA_val,A_mat->val,num_val*sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(db,b_mat->matrix,num_row*sizeof(double),cudaMemcpyHostToDevice);

    // invoke the kernel
    double* swap;
    while ((iter<max_iter)&&(error>tol)) {
        cudaSparseJacobiKernel<<<numBlock,threads_per_block>>>(dA_row_start, dA_col, dA_val, db, dx1, dx2, num_row, num_val);

        swap = dx1; 
        dx1 = dx2;
        dx2 = swap;
        iter++;
    }

    // copy result back to host
    cudaFree(dA_row_start);
    cudaFree(dA_col);
    cudaFree(dA_val);
    cudaFree(db);
    cudaFree(dx2);
    double* ret = new double[num_row];
    cudaMemcpy(ret,dx1,num_row*sizeof(double),cudaMemcpyDeviceToHost);
    cudaFree(dx1);

    // create the return struct
    matrixInfo* return_matrix = new matrixInfo;
    return_matrix->num_row = num_row;
    return_matrix->num_col = 1;
    return_matrix->matrix = ret;

    return return_matrix;

}
