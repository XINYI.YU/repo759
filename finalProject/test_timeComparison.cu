#include <stdio.h>
#include <string>
#include <iostream>

#include <cublas_v2.h>
#include <cusolverDn.h>

#include "inOutFuncs.cuh"
#include "serialFuncs.cuh"
#include "cudaFuncs.cuh"
#include "cublasFuncs.cuh"

int main(int argc, char* argv[]) {

    const size_t num_threads = atoi(argv[1]);
    // cuda timer
    cudaEvent_t start1;
    cudaEvent_t stop1;
    cudaEventCreate(&start1);
    cudaEventCreate(&stop1);

    float ms1;
    float ms2;
    float ms3;
    float ms4;
    float ms5;
    float ms6;

    matrixInfo* a = readMatrixFromText("./matrixData/A,n=2500.txt");
    matrixInfo* b = readMatrixFromText("./matrixData/b,n=2500.txt");
    std::cout << "Finish Reading A and b\n";
    
    matrixInfo* a_serialLU = a;
    matrixInfo* a_serialJacobi = a;
    matrixInfo* a_cudaLU = a;
    matrixInfo* a_cudaJacobi = a;
    matrixInfo* a_cublasLU= a;
    matrixInfo* a_cublasJacobi = a;

    // Time Calculate for serail LU Decomposition
    cudaEventRecord(start1);
    serialLUdecomp(a_serialLU);
    matrixInfo* sol1 = LUSolve(a_serialLU, b);
    cudaEventRecord(stop1);
    cudaEventSynchronize(stop1);
    cudaEventElapsedTime(&ms1, start1, stop1);

    // Time Calculate for serail Jacobi Solver
    cudaEventRecord(start1);
    matrixInfo* sol2 = serialJacobiSolve(a_serialJacobi, b, 1000, 1e-5);
    cudaEventRecord(stop1);
    cudaEventSynchronize(stop1);
    cudaEventElapsedTime(&ms2, start1, stop1);

    // Time Calculate for cuda LU Decomposition
    cudaEventRecord(start1);
    cudaLUDecomposition(a, num_threads);
    matrixInfo* sol3 = LUSolve(a_cudaLU, b);
    cudaEventRecord(stop1);
    cudaEventSynchronize(stop1);
    cudaEventElapsedTime(&ms3, start1, stop1);

    // Time Calculate for cuda Jacobi Solver
    cudaEventRecord(start1);
    matrixInfo* sol4 = cudaJacobiSolve(a_cudaJacobi, b, 1000, 1e-5, num_threads);
    cudaEventRecord(stop1);
    cudaEventSynchronize(stop1);
    cudaEventElapsedTime(&ms4, start1, stop1);
    
    // Time Calculate for cublas LU Decomposition
    cudaEventRecord(start1);
    matrixInfo* sol5 = cublasLUDecomposition(a_cublasLU, b, num_threads);
    cudaEventRecord(stop1);
    cudaEventSynchronize(stop1);
    cudaEventElapsedTime(&ms5, start1, stop1);

    // Time Calculate for cublas Jacobi Solver
    cudaEventRecord(start1);
    matrixInfo* sol6 = cublasJacobiSolve(a_cublasJacobi, b, 1000, 1e-5, num_threads);
    cudaEventRecord(stop1);
    cudaEventSynchronize(stop1);
    cudaEventElapsedTime(&ms6, start1, stop1);


    // std::cout << "Computational Time of serial LU Decomposition: " << ms1 << "\n";
    // std::cout << "Computational Time of serial Jacobi Solver: "    << ms2 << "\n";
    std::cout << "Computational Time of cuda LU Decomposition: "   << ms3 << "\n";
    std::cout << "Computational Time of cuda Jacobi Solver: "      << ms4 << "\n";
    std::cout << "Computational Time of cublas LU Decomposition: " << ms5 << "\n";
    std::cout << "Computational Time of cublas Jacobi Solver: "    << ms6 << "\n";

    const std::string filename3 = "sol3Matrix.txt";
    saveMatrixToFile(sol3, filename3);
    const std::string filename4 = "sol4Matrix.txt";
    saveMatrixToFile(sol4, filename4);
    const std::string filename5 = "sol5Matrix.txt";
    saveMatrixToFile(sol5, filename5);
    const std::string filename6 = "sol6Matrix.txt";
    saveMatrixToFile(sol6, filename6);

    delete [](sol1->matrix);
    delete sol1; 
    delete [](sol2->matrix);
    delete sol2; 

    delete [](a->matrix);
    delete a;
    delete [](b->matrix);
    delete b;

    return 0;
}