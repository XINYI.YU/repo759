#ifndef INOUTFUNCS_CUH
#define INOUTFUNCS_CUH

#include <string>
#include <iostream>
#include <fstream> 
#include <string>  

struct matrixInfo {
    unsigned int num_row;
    unsigned int num_col;
    double*      matrix;
};

matrixInfo* readMatrixFromText(std::string fileName);

void printMatrix(matrixInfo* matrix);

struct sparseMatrixInfo {
    unsigned int    num_row;
    unsigned int    num_val;
    unsigned int*   col_index;
    unsigned int*   row_start_index;
    double*         val;
};

sparseMatrixInfo* readSparseMatrixFromTxt(std::string filename);

void saveMatrixToFile(const matrixInfo* matrixData, const std::string& filename) ;

#endif