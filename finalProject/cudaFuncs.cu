#include <cuda.h>
#include <stdio.h>

#include "inOutFuncs.cuh"
#include "serialFuncs.cuh"
#include "cudaFuncs.cuh"

__global__ void cudaJacobiKernel(double* A, double* b, double* x1, double* x2, unsigned int n) {
    unsigned int index = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int start_entry = index * n;

    if (index < n) {
        double sum = 0.0;
        for (unsigned int m1 = 0; m1 < index; m1++) {
            sum += A[start_entry + m1] * x1[m1];
        }
        
        for (unsigned int m1=index+1; m1<n; m1++) {
            sum += A[start_entry + m1] * x1[m1];
        }

        x2[index] = (b[index] - sum) / A[start_entry + index];
    }

    return;
}

__host__ matrixInfo* cudaJacobiSolve(matrixInfo* A_mat, matrixInfo* b_mat, unsigned int max_iter, double tol, unsigned int threads_per_block) {
    unsigned int iter = 0;
    double error = 100.0;

    unsigned int num_row = A_mat->num_row;
    unsigned int numBlock = (num_row + threads_per_block - 1) / threads_per_block;

    //Allocate the Device Memory
    double* dA;  cudaMalloc((void**)&dA,  num_row*num_row*sizeof(double));
    double* db;  cudaMalloc((void**)&db,  num_row*sizeof(double));
    double* dx1; cudaMalloc((void**)&dx1, num_row*sizeof(double));
    double* dx2; cudaMalloc((void**)&dx2, num_row*sizeof(double));

    //Copy Input Array from Host to Device
    cudaMemcpy(dA,A_mat->matrix,num_row*num_row*sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(db,b_mat->matrix,num_row*sizeof(double),cudaMemcpyHostToDevice);

    //Invoke the Kernal
    double* swap;
    while ((iter < max_iter) && (error > tol)) {
        cudaJacobiKernel<<<numBlock,threads_per_block>>>(dA, db, dx1, dx2, num_row);
        swap = dx1; 
        dx1 = dx2;
        dx2 = swap;
        iter++;
    }

    //Copy Result back to Host
    cudaFree(dA);
    cudaFree(db);
    cudaFree(dx2);
    double* ret = new double[num_row];
    cudaMemcpy(ret,dx1,num_row*sizeof(double),cudaMemcpyDeviceToHost);
    cudaFree(dx1);

    //Create the Return Struct
    matrixInfo* return_matrix = new matrixInfo;
    return_matrix->num_row = num_row;
    return_matrix->num_col = 1;
    return_matrix->matrix = ret;

    return return_matrix;
}

__global__ void cudaLUDecompositionKernel(double* A, unsigned int n, unsigned int row_index) {
    unsigned int total_valid_num   = n - row_index;
    unsigned int start_entry_index = row_index * n + row_index;
    unsigned int row_start_index   = row_index * n;
    unsigned int global_index      = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int m2 = global_index + row_index;

    if (global_index < total_valid_num) {
        // Upper Matrix
        double sum = A[start_entry_index + global_index];
        for (unsigned int m3 = 0; m3 < row_index; m3++) {
            sum -= A[row_start_index + m3] * A[m3 * n + m2];
        }

        A[start_entry_index + global_index] = sum;
        __syncthreads();

        // Lower Matrix
        if (total_valid_num > 1) {
            if (global_index > 0) {
                double sum = A[m2 * n + row_index];
                for (unsigned int m3 = 0; m3 < row_index; m3++) {
                    sum -= A[m2 * n + m3] * A[m3*n + row_index];
                }
                A[m2 * n + row_index] = sum / A[start_entry_index];
            }
        }
        __syncthreads();
    }     
}

__host__ void cudaLUDecomposition(matrixInfo* A_mat, unsigned int threads_per_block) {
    double* A = A_mat->matrix;
    unsigned int num_row = A_mat->num_row;
    unsigned int numBlock;

    // Allocate the Device Memory
    double* dA;  cudaMalloc((void**)&dA, num_row * num_row * sizeof(double));

    // Copy Input Array from Host to Device
    cudaMemcpy(dA,A,num_row * num_row * sizeof(double),cudaMemcpyHostToDevice);

    // Invoke LU Decomposition Kernal
    for (unsigned int m1 = 0; m1 < num_row; m1++) {
        numBlock = (num_row - m1+threads_per_block - 1) / threads_per_block;
        cudaLUDecompositionKernel<<<numBlock,threads_per_block>>>(dA, num_row, m1);
    } 

    // Copy the Decomposed Matrix Backt to Host
    cudaMemcpy(A,dA,num_row*num_row*sizeof(double),cudaMemcpyDeviceToHost);
    cudaFree(dA);
}