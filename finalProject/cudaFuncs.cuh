#ifndef CUDAFUNCS_CUH
#define CUDAFUNCS_CUH

#include <cuda.h>
#include "inOutFuncs.cuh"
#include "serialFuncs.cuh"

__global__ void cudaJacobiKernel(double* A, double* b, double* x1, double* x2, unsigned int n);

__host__ matrixInfo* cudaJacobiSolve(matrixInfo* A, matrixInfo* b, unsigned int max_iter, double tol, unsigned int threads_per_block);

__global__ void cudaLUDecompositionKernel(double* A, unsigned int n, unsigned int row_index);

__host__ void cudaLUDecomposition(matrixInfo* A_mat, unsigned int threads_per_block);

#endif