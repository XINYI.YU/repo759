import numpy as np

# Define the matrix A
A = np.array([[2,4,1],
              [3,5,2],
              [1,3,1]])

# Define the vector b
b = np.array([7,10,4])

# Solve for x
x = np.linalg.solve(A, b)

print("Solution x:")
print(x)
