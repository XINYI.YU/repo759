import random

def generate_matrix(rows, columns):
    matrix = [[random.randint(1, 100) for _ in range(columns)] for _ in range(rows)]
    return matrix

def write_matrix_to_file(matrix, filename):
    with open(filename, 'w') as file:
        file.write(f"{len(matrix)}\n")
        file.write(f"{len(matrix[0])}\n")
        for row in matrix:
            file.write(' '.join(map(str, row)) + '\n')

# Set the desired number of rows and columns for the matrix
num_rows = 3
num_columns = 3

# Generate the matrix
matrix = generate_matrix(num_rows, num_columns)

# Write the matrix to a text file
file_name = "matrix_A.txt"
write_matrix_to_file(matrix, file_name)
print(f"Matrix of size {num_rows}x{num_columns} generated and saved to {file_name}.")
