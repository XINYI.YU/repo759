#include "inOutFuncs.cuh"
#include <iostream>
#include <fstream> 
#include <string>  
#include <stdio.h>


matrixInfo* readMatrixFromText(std::string fileName) {
    char buffer[64];
    char ch;
    unsigned int num_row = 0, num_col = 0, current_index, inword_index;
    bool in_word;
    double data;
    double* ret_mat = NULL;

    current_index = 0;
    in_word = true;
    inword_index = 0;

    // Open the Target File
    FILE* infile = fopen(fileName.c_str(),"r");
    while((ch = fgetc(infile)) != EOF) {
        if (in_word == true) {
            if ((ch == ' ') || (ch == '\n') || (ch == '\t') || (ch == 13)) {
                in_word = false;
                buffer[inword_index] = '\0';
                data = atof(buffer);
                inword_index = 0;
                if (current_index == 0) {
                    num_row = (unsigned int) data;
                }
                else {
                    if (current_index == 1) {
                        num_col = (unsigned int) data;
                        ret_mat = new double[num_row * num_col];
                    }
                    else {
                        ret_mat[current_index - 2] = data;
                    }
                }
                current_index++;
            }
            else {
                buffer[inword_index] = ch;
                inword_index++;
            }
        }
        else {
            if ((ch!=' ') && (ch!='\n') && (ch!='\t') && (ch!=13)) {
                in_word = true;
                buffer[inword_index] = ch;
                inword_index++;
            }
        }
    }
    fclose(infile);

    matrixInfo* ret = new matrixInfo;
    ret->num_row = num_row;
    ret->num_col = num_col;
    ret->matrix = ret_mat;

    return ret;
}

void printMatrix(matrixInfo* a) {
    unsigned int num_row = a->num_row;
    unsigned int num_col = a->num_col;
    double* mat = a->matrix;
    std::cout << "Number of Rows = " << num_row << ", Number of Columns = " << num_col << "\n";
    for (unsigned int m1=0; m1<num_row; m1++) {
        for (unsigned int m2=0; m2<num_col; m2++) std::cout << mat[m1*num_col+m2] << " ";
        std::cout << "\n";
    }
    std::cout << "\n";
}


sparseMatrixInfo* readSparseMatrixFromTxt(std::string filename) {
    char buffer[64];
    char ch;
    unsigned int num_row=0, num_val=0, current_index, inword_index;
    bool in_word;
    double data;
    unsigned int* row_index_arr=NULL;
    unsigned int* col_index_arr=NULL;
    unsigned int* row_start_index_arr=NULL;
    double* val_arr=NULL;

    current_index = 0;
    in_word = true;
    inword_index = 0;
    // open the target file
    FILE* infile = fopen(filename.c_str(), "r");
    while((ch=fgetc(infile))!=EOF) {
        if (in_word==true) {
            // currently inside a number
            if ((ch==' ')||(ch=='\n')||(ch=='\t')||(ch==13)) {
                // the end of a whole number
                in_word = false;
                buffer[inword_index] = '\0';
                data = atof(buffer);
                inword_index = 0;
                if (current_index == 0)         num_row = (unsigned int)data;
                else {
                    if (current_index == 1) {
                        num_val = (unsigned int)data;
                        row_index_arr = new unsigned int[num_val];
                        col_index_arr = new unsigned int[num_val];
                        val_arr = new double[num_val];
                    }
                    else {
                        if (((current_index-2)/num_val)==0)      row_index_arr[(current_index-2)%num_val]=(unsigned int)data;
                        else {
                            if (((current_index-2)/num_val)==1)  col_index_arr[(current_index-2)%num_val]=(unsigned int)data;
                            else                                 val_arr[(current_index-2)%num_val]=data;
                        }
                    }
                }
                current_index++;
            }
            else {
                // reading chars for a number
                buffer[inword_index] = ch;
                inword_index++;
            }
        }
        else {
            // currently outside a number
            if ((ch!=' ')&&(ch!='\n')&&(ch!='\t')&&(ch!=13)) {
                in_word = true;
                buffer[inword_index] = ch;
                inword_index++;
            }
        }
    }

    // close the target file
    fclose(infile);

    // create start index
    row_start_index_arr = new unsigned int[num_row];
    row_start_index_arr[0] = 0;
    for (unsigned int m1=1; m1<num_val; m1++) {
        if (row_index_arr[m1]!=row_index_arr[m1-1]) {
            row_start_index_arr[row_index_arr[m1]] = m1;
        }
    }
    
    delete []row_index_arr;

    sparseMatrixInfo* ret = new sparseMatrixInfo;
    ret->num_row = num_row;
    ret->num_val = num_val;
    ret->row_start_index  = row_start_index_arr;
    ret->col_index  = col_index_arr;
    ret->val  = val_arr;

    return ret;
}

void saveMatrixToFile(const matrixInfo* matrixData, const std::string& filename) {
    std::ofstream file(filename, std::ios::out | std::ios::binary);

    if (file.is_open()) {
        file << matrixData->num_row << " " << matrixData->num_col << "\n";
        for (unsigned int i = 0; i < matrixData->num_row; ++i) {
            for (unsigned int j = 0; j < matrixData->num_col; ++j) {
                file << matrixData->matrix[i * matrixData->num_col + j] << " ";
            }
            file << "\n";
        }
        file.close();
        std::cout << "Saved to " << filename << " successfully." << std::endl;
    } else {
        std::cerr << "Unable to open file " << filename << " for writing." << std::endl;
    }
}