#ifndef SERIALFUNCS_CUH
#define SERIALFUNCS_CUH

#include <string>
#include "inOutFuncs.cuh"

void serialLUdecomp(matrixInfo* matrix);

matrixInfo* LUSolve(matrixInfo* A, matrixInfo* b);

matrixInfo* serialJacobiSolve(matrixInfo* A, matrixInfo* b, unsigned int max_iter, double tol);

#endif