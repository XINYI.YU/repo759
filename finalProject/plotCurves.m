clear; close all; clc
data = [
    1381.49 886.40 685.69 680.70 687.29 686.17 700.49 717.81 1136.43 2101.40;
    1330.87 691.65 488.38 485.87 492.89 499.67 500.56 535.17 850.26 1920.21;
    88.57 86.23 86.02 85.98 86.38 86.33 86.05 86.59 86.05 86.03;
    204.78 187.85 184.35 184.84 183.53 184.11 183.83 188.32 192.04 185.16
];

x = (1:size(data, 2)); % x-axis data (column numbers)
colors = ['r', 'g', 'b', 'm']; % colors for the plots
legend_labels = {'cuda LU', 'cuda Jacobi', 'cuBLAS LU', 'cuBLAS Jacobi'}; % legend labels for each row

figure;
hold on;

for i = 1:size(data, 1)
    semilogy(x, data(i, :), 'Color', colors(i), 'LineWidth', 1.5);
end

% title('Computational Time with Different Number of Threads');
xlabel('Power of 2');
ylabel('Computational Time(ms)');
legend(legend_labels);
grid on

hold off;
